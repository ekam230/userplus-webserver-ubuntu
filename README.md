### Системные требования
```
Ubuntu 16.04 x64
RAM 1Gb
Disk 5Gb
```
### Содержимое веб-сервера
```
Nginx
PHP-FPM 5.6/7.0
MySQL
PhpMyAdmin
VsFTP
Lshell
```
### Базовая настройка веб-сервера
```
#!bash

nano setup.sh
sh setup.sh
```

###Содержимое файла setup.sh
```
#!/bin/bash
apt update && apt install git ansible -y
git clone https://Drobkov@bitbucket.org/Drobkov/userplus-webserver-ubuntu.git
rm -rf userplus-webserver-ubuntu/.git
MYSQLROOTPASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 9 | head -n 1)
sed -i "s/password/$MYSQLROOTPASS/" userplus-webserver-ubuntu/vars/main.yml
ansible-playbook -i  userplus-webserver-ubuntu/hosts userplus-webserver-ubuntu/start.yml
```

###Будет создана директория userplus-webserver-ubuntu

```
#!bash

cd userplus-webserver-ubuntu
```


###Добавить сайт в интерактивном режиме

```
#!bash

ansible-playbook add.yml
```


###Добавить сайт, указав значения переменных

```
#!bash

ansible-playbook add.yml --extra-vars "username=sanes userpass=P@ssWord mysql_user_pass=P@ssWord domain=srv1.domain.dev cms=modx"
```


###Удалить пользователя и сайт

```
#!bash

ansible-playbook del.yml --extra-vars "username=sanes"
```